package info;

import java.util.ArrayList;
import java.util.HashMap;

public class ArraysDemo {

	public static void main(String[] args)
	{
	ArrayList l1 = new ArrayList();
	l1.add(2);
	l1.add(33);
	l1.add(333);
	l1.add(44);
	l1.add(5);
	l1.add(7);
	l1.add(9);
	l1.add(21);
	l1.add(11);
	l1.add(null);
	System.out.println(l1.size());

	String str = "I am from from bangalore bangalore nikit nikit";
	String[] arr =str.split(" ");
	HashMap<String, Integer> map = new HashMap<String,Integer>();
	for (String c  :arr)
	{
		if(map.containsKey(c))
		{
			map.put(c, map.get(c)+1);
		}
		else
		{
			map.put(c, 1);
		}
	}
		
	System.out.println(map);
	}

}
